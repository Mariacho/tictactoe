﻿using System;
using System.Collections;
using App.Scripts.MessageSystem;
using Unity.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace App.Scripts
{
    public class SceneLoader : MonoBehaviour
    {
        private static SceneLoader _instance;

        private void Awake()
        {
            if (!_instance)
                _instance = this;
            else
            {
                Debug.LogError("You have more than one SceneLoader!");
            }
        }

        void Start()
        {
            MessagingSystem.Instance.AttachListener(typeof(LoadSceneMessage), this.HandleLoadSceneMessage);
        }

        private void OnDestroy()
        {
            MessagingSystem.Instance.DetachListener(typeof(LoadSceneMessage), this.HandleLoadSceneMessage);
        }
        
        bool HandleLoadSceneMessage(BaseMessage msg)
        {
            var castMsg = (LoadSceneMessage) msg;
            StartCoroutine(ReactionMessage(castMsg));
            return true;
        }

        IEnumerator ReactionMessage(LoadSceneMessage msg)
        {
            yield return new WaitForEndOfFrame();
            if (msg.Reload)
                ReloadScene();

            if (msg.Preload)
                PreLoadScene(msg.SceneName);
            else
                LoadScene(msg.SceneName);
        }

        public void LoadScene(int index)
        {
            SceneManager.LoadScene(index, LoadSceneMode.Single);
        }

        public void LoadScene(string nameScene)
        {
            SceneManager.LoadScene(nameScene, LoadSceneMode.Single);
        }

        public void ReloadScene()
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().name, LoadSceneMode.Single);
        }

        public void PreLoadScene(int index)
        {
            SceneManager.LoadSceneAsync(index).allowSceneActivation = false;
        }

        public void PreLoadScene(string nameScene)
        {
            SceneManager.LoadSceneAsync(nameScene).allowSceneActivation = false;
        }
        
    }
}