﻿using UnityEngine;
using UnityEngine.UI;

namespace App.Scripts.UI
{
    public class MenuUILinks : MonoBehaviour
    {
        [Header("MenuScreen")] 
        public Button Start;
        public Button Exit;
    }
}