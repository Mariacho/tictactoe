﻿using App.Scripts.MessageSystem;
using UnityEngine;

namespace App.Scripts.UI
{
    public class MenuUIButtonsCallBack : MonoBehaviour
    {
        [SerializeField] private MenuUILinks _menuUiLinks;
        
        private void Awake()
        {
            _menuUiLinks.Start.onClick.AddListener(() => ClickStartGame());
            _menuUiLinks.Exit.onClick.AddListener(() => ClickExit());
        }

        private void ClickStartGame()
        {
            MessagingSystem.Instance.QueueMessage(new LoadSceneMessage("Find", false, false));
        }

        private void ClickExit()
        {
            Application.Quit();
            Debug.Log("Quit");
        }
    }
}