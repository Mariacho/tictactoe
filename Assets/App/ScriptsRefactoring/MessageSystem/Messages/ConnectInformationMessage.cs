﻿namespace App.Scripts.MessageSystem
{
    public class ConnectInformationMessage : BaseMessage
    {
        public readonly string Message;

        public ConnectInformationMessage(string message)
        {
            Message = message;
        }
    }
}