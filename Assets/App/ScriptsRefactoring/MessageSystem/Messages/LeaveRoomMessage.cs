﻿namespace App.Scripts.MessageSystem
{
    public class LeaveRoomMessage : BaseMessage
    {
        public readonly bool PlayAgain;

        public LeaveRoomMessage(bool playAgain)
        {
            PlayAgain = playAgain;
        }
    }
}