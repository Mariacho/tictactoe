﻿namespace App.Scripts.MessageSystem
{
    public class LoadSceneMessage : BaseMessage
    {
        public readonly string SceneName;
        public readonly bool Preload;
        public readonly bool Reload;

        public LoadSceneMessage(string sceneName, bool preload, bool reload)
        {
            SceneName = sceneName;
            Preload = preload;
            Reload = reload;
        }
    }
}