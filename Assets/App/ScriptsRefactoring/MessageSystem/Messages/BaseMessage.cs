﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace App.Scripts.MessageSystem
{
    public class BaseMessage
    {
        public string Name;

        public BaseMessage()
        {
            Name = this.GetType().Name;
        }
    }
}