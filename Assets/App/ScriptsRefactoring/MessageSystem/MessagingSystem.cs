﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;

namespace App.Scripts.MessageSystem
{
    public delegate bool MessageHandlerDelegate(BaseMessage message);
    
    public class MessagingSystem : MonoBehaviour
    {
        public static MessagingSystem Instance;
        
        private Queue<BaseMessage> _messageQueue = new Queue<BaseMessage>();
        private Dictionary<string, List<MessageHandlerDelegate>> _listenerDict = new Dictionary<string, List<MessageHandlerDelegate>>();
        [Range(0, 0.2f)]
        [SerializeField] private float _maxQueueProcessingTime = 0.1f;
        
        #region Private methods

        private void Awake()
        {
            if (Instance == null)
            {
                Instance = this;
            }
            else
            {
                Debug.LogError($"You have more than one {this.name} in the scene! You" +
                               $"only need 1, it's a singleton!");
                Destroy(this);
            }
        }

        private void Update()
        {
            float timer = 0.0f;
            while (_messageQueue.Count > 0)
            {
                if (_maxQueueProcessingTime > 0.0f)
                {
                    if (timer > _maxQueueProcessingTime)
                        return;
                }

                BaseMessage msg = _messageQueue.Dequeue();
                if (!TriggerMessage(msg))
                    Debug.LogError($"Error when processing message: {msg.Name}");

                if (_maxQueueProcessingTime > 0.0f)
                    timer += Time.deltaTime;
            }
        }
        
        #endregion
        
        
        #region Public methods

        public bool AttachListener(System.Type type, MessageHandlerDelegate handler)
        {
            if (type == null)
            {
                Debug.Log("MessageSystem: AttachListener failed! Type not found.");
                return false;
            }

            string msgName = type.Name;
            if (!_listenerDict.ContainsKey(msgName))
            {
                _listenerDict.Add(msgName, new List<MessageHandlerDelegate>());
            }

            List<MessageHandlerDelegate> listenerList = _listenerDict[msgName];
            if (listenerList.Contains(handler))
            {
                return false; //Получатель уже есть в списке
            }

            listenerList.Add(handler);
            return true;
        }

        public bool DetachListener(System.Type type, MessageHandlerDelegate handler)
        {
            if (type == null)
            {
                Debug.Log("MessageSystem: DetachListener failed! Type not found.");
                return false;
            }

            string msgName = type.Name;

            if (!_listenerDict.ContainsKey(type.Name))
            {
                return false;
            }

            List<MessageHandlerDelegate> listenerList = _listenerDict[msgName];
            if (!listenerList.Contains(handler))
            {
                return false;
            }

            listenerList.Remove(handler);
            return true;
        }

        public bool QueueMessage(BaseMessage msg)
        {
            if (!_listenerDict.ContainsKey(msg.Name))
            {
                return false;
            }
            _messageQueue.Enqueue(msg);
            return true;
        }

        public bool TriggerMessage(BaseMessage msg)
        {
            string msgName = msg.Name;
            if (!_listenerDict.ContainsKey(msgName))
            {
                Debug.Log($"MessagingSystem: Message {msgName} has no listeners!");
                return false;
            }
        
            List<MessageHandlerDelegate> listenerList = _listenerDict[msgName];
            foreach (var handlerDelegate in listenerList)
            {
                handlerDelegate(msg);
            }
            return true;
        }

        #endregion
    }
    
}