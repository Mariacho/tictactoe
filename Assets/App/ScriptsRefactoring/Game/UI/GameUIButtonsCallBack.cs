﻿using App.Scripts.MessageSystem;
using UnityEngine;

namespace App.Scripts.UI
{
    public class GameUIButtonsCallBack : MonoBehaviour
    {
        [SerializeField] private GameUILinks _gameUiLinks;

        private void Awake()
        {
            _gameUiLinks.GameButtonMenu.onClick.AddListener(() => ClickGameMenu());
            _gameUiLinks.MenuBunttonContinue.onClick.AddListener(() => ClickContinue());
            _gameUiLinks.MenuButtonMenu.onClick.AddListener(() => ClickMainMenu());
            _gameUiLinks.EndGameButtonMenu.onClick.AddListener(() => ClickMainMenu());
            _gameUiLinks.EndGameButtonPlayAgain.onClick.AddListener(() => ClickPlayAgain());
        }

        private void ClickMainMenu()
        {
            MessagingSystem.Instance.QueueMessage(new LeaveRoomMessage(false));
        }

        private void ClickGameMenu()
        {
            _gameUiLinks.MenuScreen.SetActive(true);
        }

        private void ClickContinue()
        {
            _gameUiLinks.MenuScreen.SetActive(false);
        }

        private void ClickPlayAgain()
        {
            MessagingSystem.Instance.QueueMessage(new LeaveRoomMessage(true));
        }
    }
}