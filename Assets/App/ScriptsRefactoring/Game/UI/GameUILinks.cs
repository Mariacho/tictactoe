﻿using UnityEngine;
using UnityEngine.UI;

namespace App.Scripts.UI
{
    public class GameUILinks : MonoBehaviour
    {
        [Header("GameScreen")]
        public Button GameButtonMenu;
        public Text GameTextTurnInfo;
        
        [Header("MenuScreen")]
        public GameObject MenuScreen;
        public Button MenuBunttonContinue;
        public Button MenuButtonMenu;

        [Header("EndGameScreen")]
        public GameObject EndGameScreen;
        public Text EndGameTextResultGame;
        public Button EndGameButtonPlayAgain;
        public Button EndGameButtonMenu;
    }
}