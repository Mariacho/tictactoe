﻿using App.Scripts.GameLogic;

namespace App.ScriptsRefactoring.Game
{
    public class TurnMaker
    {
        private TurnQueue _turnQueue;
        private bool _isActive;
        public TurnMaker(TurnQueue turnQueue)
        {
            _turnQueue = turnQueue;
        }

        public void Make(Cell cell)
        {
            cell.CurrentState = _turnQueue.GetState();
        }
    }
}