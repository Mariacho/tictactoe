﻿using System;
using App.Scripts.GameLogic;
using App.ScriptsRefactoring.Game.GridAndCellsLogic;
using App.ScriptsRefactoring.Game.InputSystems;

namespace App.ScriptsRefactoring.Game
{
    public class GameLoop
    {
        public readonly GameState GameState;
        private bool _isPlaying;
        
        private BaseInput _input;
        private TurnPossibleChecker _possibleChecker;
        private EndGameChecker _endGame;
        private TurnMaker _turnMaker;
        private TurnQueue _queue;
        private TurnDrawer _drawer;
        //add audio player?
        
        public GameLoop(BaseInput input, bool areYouPlayX)
        {
            GameState = new GameState();
            
            _isPlaying = true;
            _possibleChecker = new TurnPossibleChecker();
            _endGame = new EndGameChecker(GameState, areYouPlayX);
            _queue = new TurnQueue();
            _turnMaker = new TurnMaker(_queue);
            _drawer = new TurnDrawer();

            _input = input;
            _input.TryMakeTurn += TryMakeTurn;
        }

        ~GameLoop()
        {
            _input.TryMakeTurn -= TryMakeTurn;
        }
        
        private void TryMakeTurn(Cell cell)
        {
            if (GameState.Current != GameStateVariants.Game)
                return;
            
            if (!_possibleChecker.Check(cell))
                return;

            _turnMaker.Make(cell);
            _drawer.DrawCell(cell);
            _endGame.CheckEndGame(cell);
        }
    }
}