﻿using App.Scripts.GameLogic;

namespace App.Scripts.Interfaces
{
    public interface ITurnHandler
    {
        void MakeTurn(Cell cell);
    }
}