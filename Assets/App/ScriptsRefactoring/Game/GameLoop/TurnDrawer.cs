﻿using App.Scripts.GameLogic;

namespace App.ScriptsRefactoring.Game
{
    public class TurnDrawer
    {
        public void DrawCell(Cell cell)
        {
            cell.Drawler.DrawCell(cell.CurrentState);
        }
    }
}