﻿using System.Collections.Generic;
using System.ComponentModel;
using App.Scripts.GameLogic;
using Unity.Mathematics;

namespace App.ScriptsRefactoring.Game
{
    /// <summary>
    /// Cell near positions
    /// -------------------
    /// | 0-0 | 1-0 | 2-0 |
    /// -------------------
    /// | 0-1 |(1-1)| 2-1 |
    /// -------------------
    /// | 0-2 | 1-2 | 2-2 |
    /// -------------------
    ///
    /// (1-1) - This Cell
    /// </summary>
    
    public enum GameStateVariants
    {
        Game,
        Win,
        Lose,
        Draw
    }

    public class NewGameRules
    {
        private List<Cell> _checkedCells = new List<Cell>();
        public GameStateVariants CheckEndGame(Cell cell)
        {
            if (!CheckAround(cell))
                return GameStateVariants.Game;

            if (CheckHorizontal(cell) ||
                CheckVertical(cell) ||
                CheckDiagonal(cell))
                return GameStateVariants.Win;

            if (CheckDraw(cell))
                return GameStateVariants.Draw;
            
            return GameStateVariants.Game;
        }

        private bool CheckAround(Cell cell)
        {
            foreach (var nearCell in cell.NearCells)
            {
                if (cell.CurrentState == nearCell?.CurrentState)
                    return true;
            }

            return false;
        }

        private bool CheckHorizontal(Cell cell)
        {
            return CheckTwoIndexNeighbors(cell, new int2(0, 1), new int2(2, 1));
        }

        private bool CheckVertical(Cell cell)
        {
            return CheckTwoIndexNeighbors(cell, new int2(1, 0), new int2(1, 2));
        }

        private bool CheckDiagonal(Cell cell)
        {
            return CheckTwoIndexNeighbors(cell, new int2(0, 0), new int2(2, 2)) ||
                   CheckTwoIndexNeighbors(cell, new int2(0, 2), new int2(2, 0));
        }

        private bool CheckTwoIndexNeighbors(Cell cell, int2 pos1, int2 pos2)
        {
            return CheckTwoNeighbors(cell, cell?.NearCells[pos1.x, pos1.y], cell?.NearCells[pos2.x, pos2.y]) ||
                   CheckTwoNeighbors(cell, cell?.NearCells[pos1.x, pos1.y], cell?.NearCells[pos1.x, pos1.y].NearCells[pos1.x, pos1.y]) ||
                   CheckTwoNeighbors(cell, cell?.NearCells[pos2.x, pos2.y], cell?.NearCells[pos2.x, pos2.y].NearCells[pos2.x, pos2.y]);
        }

        private bool CheckTwoNeighbors(Cell cell, Cell neighbor1, Cell neighbor2)
        {
            return cell.CurrentState == neighbor1.CurrentState &&
                   cell.CurrentState == neighbor2.CurrentState;
        }

        private bool CheckDraw(Cell cell)
        {
            _checkedCells.Clear();
            AddNearToChecked(cell);
            for (int i = 0; i < _checkedCells.Count; i++)
            {
                if (_checkedCells[i].CurrentState == Cell.CellState.empty)
                    return false;
            }
            return true;
        }

        private void AddNearToChecked(Cell cell)
        {
            _checkedCells.Add(cell);

            foreach (var nearCell in cell.NearCells)
            {
                if (!_checkedCells.Contains(nearCell) && nearCell != null)
                    AddNearToChecked(nearCell);
            }
        }
    }

}


