﻿using System.Net.NetworkInformation;
using App.Scripts.GameLogic;

namespace App.ScriptsRefactoring.Game
{
    public class TurnQueue
    {
        private bool _isX;
        public Cell.CellState GetState()
        {
            _isX = !_isX;
            if (_isX)
                return Cell.CellState.x;
            else
                return Cell.CellState.o;
        }
    }
}