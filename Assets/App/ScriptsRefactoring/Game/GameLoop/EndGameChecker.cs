﻿using System;
using App.Scripts.GameLogic;

namespace App.ScriptsRefactoring.Game
{
     public class EndGameChecker
    {
        private NewGameRules _gameRules;
        private GameStateVariants _checkResult;

        private readonly bool _youPlayerWhoPlaysX;
        private GameState _gameState;

        public EndGameChecker(GameState game, bool playsX)
        {
            _gameRules = new NewGameRules();
            _youPlayerWhoPlaysX = playsX;
            _gameState = game;
        }

        public void CheckEndGame(Cell cell)
        {
            _checkResult = _gameRules.CheckEndGame(cell);

            switch (_checkResult)
            {
                case GameStateVariants.Game:
                    //
                    break;
                case GameStateVariants.Win:
                    CheckWhoWin(cell);
                    break;
                case GameStateVariants.Lose:
                    //
                    break;
                case GameStateVariants.Draw:
                    _gameState.SetState(GameStateVariants.Draw);
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private void CheckWhoWin(Cell cell)
        {
            if (IsYouWin(cell))
                _gameState.SetState(GameStateVariants.Win);
            else
                _gameState.SetState(GameStateVariants.Lose);
        }

        private bool IsYouWin(Cell cell)
        {
            if (cell.CurrentState == Cell.CellState.x && _youPlayerWhoPlaysX)
                return true;
            return false;
        }
    }
}