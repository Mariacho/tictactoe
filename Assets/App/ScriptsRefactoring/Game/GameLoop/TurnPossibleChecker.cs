﻿using App.Scripts.GameLogic;

namespace App.ScriptsRefactoring.Game
{
    public class TurnPossibleChecker
    {
        public bool Check(Cell cell)
        {
            return cell.CurrentState == Cell.CellState.empty;
        }
    }
}