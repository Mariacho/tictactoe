﻿namespace App.ScriptsRefactoring.Game
{
    public class GameState
    {
        public delegate void GetState(GameStateVariants newState);
        public event GetState ChangedState;

        public GameStateVariants Current { get; private set; }

        public void SetState(GameStateVariants newState)
        {
            Current = newState;
            ChangedState(Current);
        }
    }
}