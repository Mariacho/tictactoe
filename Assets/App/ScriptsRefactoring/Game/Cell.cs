﻿using System;
using System.Collections.Generic;
using App.ScriptsRefactoring.Game;
using UnityEngine;
using UnityEngine.UI;

namespace App.Scripts.GameLogic
{
    [Serializable]
    public class Cell
    {
        public readonly string Name;
        public readonly ICellDrawler Drawler;
        public readonly Cell[,] NearCells = new Cell[3,3];
        public CellState CurrentState;

        public enum CellState
        {
            locked,
            empty,
            o,
            x
        }

        public Cell(CellState state, ICellDrawler drawler, string name)
        {
            CurrentState = state;
            Drawler = drawler;
            Name = name;
        }
    }
}