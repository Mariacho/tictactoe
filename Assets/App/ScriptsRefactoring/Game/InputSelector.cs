﻿using System;
using System.Collections;
using System.Collections.Generic;
using App.ScriptsRefactoring.Game.GridAndCellsLogic;
using Photon.Pun;
using UnityEngine;

namespace App.ScriptsRefactoring.Game.InputSystems
{
    public enum Inputs
    {
        Solo,
        Ai,
        PUN
    }

    public class InputSelector : MonoBehaviour
    {
        public BaseInput CurrentInput;
        [SerializeField] private Inputs SelectedInput;
        //TODO: Дописать варианты выбора инпута
        private Dictionary<Inputs, BaseInput> playerInputs;
        
        [SerializeField] private PhotonTurnSender _photonTurn;

        private void Awake()
        {
            playerInputs = new Dictionary<Inputs, BaseInput>
            {
                {Inputs.Solo, new SoloInput(true)},
                {Inputs.Ai, new SoloInput(true)},
                {Inputs.PUN, new PhotonInput()}
            };
            
            CurrentInput = playerInputs[SelectedInput];

            if (CurrentInput == playerInputs[Inputs.PUN])
                PhotonInputInitialization();
        }

        private void PhotonInputInitialization()
        {
            var photonInput = (PhotonInput)playerInputs[Inputs.PUN];
            _photonTurn.Input = photonInput;
            photonInput.PhotonTurn = _photonTurn;
            
            Debug.Log("PhotonInputInitialization!");
        }
    }
}