﻿namespace App.ScriptsRefactoring.Game
{
    public interface IConditionOfTheFirst
    {
        bool GetFirst();
    }
}