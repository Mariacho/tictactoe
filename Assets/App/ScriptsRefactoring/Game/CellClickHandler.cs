﻿using System;
using App.Scripts.GameLogic;
using App.Scripts.MessageSystem;
using App.ScriptsRefactoring.Game;
using App.ScriptsRefactoring.Game.InputSystems;
using UnityEditor;
using UnityEngine;
using UnityEngine.EventSystems;

namespace App.Scripts
{
    public class CellClickHandler : MonoBehaviour, IPointerClickHandler
    {
        [SerializeField] private InputSelector _inputSelector;
        [SerializeField] private CellUnityContainer _container;
        private Cell _cell;

        private void Awake()
        {
            _cell = _container.Cell;
        }
        
        public void OnPointerClick(PointerEventData eventData)
        {
            _inputSelector?.CurrentInput.TryClickWithCondition(_cell);
        }
    }
}