﻿using App.Scripts.GameLogic;

namespace App.ScriptsRefactoring.Game
{
    public interface ICellDrawler
    {
        void DrawCell(Cell.CellState state);
    }
}