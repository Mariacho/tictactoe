﻿using System;
using App.Scripts.GameLogic;
using UnityEngine;
using UnityEngine.UI;

namespace App.ScriptsRefactoring.Game
{
    public class UnityVizualize : MonoBehaviour, ICellDrawler
    {
        [SerializeField] private Image _outputImage;
        
        [SerializeField] private Sprite _x;
        [SerializeField] private Sprite _o;

        public void DrawCell(Cell.CellState state)
        {
            switch (state)
            {
                case Cell.CellState.locked:
                    break;
                case Cell.CellState.empty:
                    break;
                case Cell.CellState.o:
                    _outputImage.sprite = _o;
                    break;
                case Cell.CellState.x:
                    _outputImage.sprite = _x;
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(state), state, null);
            }
        }
    }
}