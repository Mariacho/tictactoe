﻿using System;
using App.Scripts.GameLogic;
using App.ScriptsRefactoring.Game;
using UnityEngine;

namespace App.ScriptsRefactoring.Game
{
    public class CellUnityContainer : MonoBehaviour
    {
        public Cell Cell { get; private set; }
        [SerializeField] private UnityVizualize _drawler;

        private void Awake()
        {
            Cell = new Cell(Cell.CellState.empty, _drawler, name);
        }
    }
}