﻿using System.Collections.Generic;
using UnityEngine;

namespace App.ScriptsRefactoring.Game.ScriptableObjects
{
    [CreateAssetMenu(fileName = "NewAudioPreset", menuName = "TicTacToy/New Audio Preset", order = 0)]
    public class AudioPreset : ScriptableObject
    {
        public AudioClip Victory;
        public AudioClip Defeat;
        public List<AudioClip> Clicks = new List<AudioClip>();
        public List<AudioClip> Menu = new List<AudioClip>();
        public List<AudioClip> Game = new List<AudioClip>();
    }
}