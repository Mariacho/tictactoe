﻿using UnityEngine;

namespace App.ScriptsRefactoring.Game.ScriptableObjects
{
    [CreateAssetMenu(fileName = "NewGridPreset", menuName = "TicTacToy/New Grid Preset", order = 0)]
    public class GridPreset : ScriptableObject
    {
        public int Rows, Lines;
    }
}