﻿using System;
using App.ScriptsRefactoring.Game;
using Photon.Pun;
using UnityEngine;

namespace App.ScriptsRefactoring.Photon
{
    public class SetFirstPlayerPUN : MonoBehaviour, IConditionOfTheFirst
    {
        [SerializeField] private UnityGameLoop _gameLoop;

        private void Awake()
        {
            _gameLoop._conditionOfTheFirst = this;
            CheckHost();
        }

        public bool GetFirst()
        {
            if (PhotonNetwork.IsMasterClient)
                return true;
            return false;
        }

        private void CheckHost()
        {
            if (GetFirst())
                Debug.Log("First Player!");
            else
                Debug.Log("Second Player");
        }
    }
}