﻿using System.Collections;
using App.Scripts.MessageSystem;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine;

namespace App.Photon
{
    public class PhotonGameCallBack : MonoBehaviourPunCallbacks
    {
        private bool _isRestart;
        private bool _isPlaying = true;
        
        void Start()
        {
            MessagingSystem.Instance.AttachListener(typeof(LeaveRoomMessage), this.HandleLeaveRoomMessage);
            MessagingSystem.Instance.AttachListener(typeof(EndGameMessage), this.HandleEndGameMessage);
        }
        
        private void OnDestroy()
        {
            MessagingSystem.Instance.DetachListener(typeof(LeaveRoomMessage), this.HandleLeaveRoomMessage);
            MessagingSystem.Instance.DetachListener(typeof(EndGameMessage), this.HandleEndGameMessage);
        }

        bool HandleLeaveRoomMessage(BaseMessage msg)
        {
            LeaveRoomMessage castMsg = msg as LeaveRoomMessage;
            LeaveRoom(castMsg.PlayAgain);
            return true;
        }

        bool HandleEndGameMessage(BaseMessage msg)
        {
            _isPlaying = false;
            return true;
        }
        
        public void LeaveRoom(bool restart)
        {
            PhotonNetwork.LeaveRoom();
            _isRestart = restart;
        }

        public override void OnPlayerLeftRoom(Player otherPlayer)
        {
            if (_isPlaying)
                LeaveRoom(false);
        }

        public override void OnLeftRoom()
        {
            Debug.Log("<color=orange>MultiplayerExitRoom</color>: OnLeftRoom");

            PhotonNetwork.Disconnect();
        }
        
        public override void OnDisconnected(DisconnectCause cause)
        {
            Debug.Log("<color=orange>MultiplayerExitRoom</color>: OnDisconnected");
            if (_isRestart)
                MessagingSystem.Instance.QueueMessage(new LoadSceneMessage("Find", false, false));
            else
                MessagingSystem.Instance.QueueMessage(new LoadSceneMessage("Menu", false, false));
        }
        
    }
}