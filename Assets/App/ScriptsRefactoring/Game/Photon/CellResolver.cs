﻿using System;
using System.Collections.Generic;
using App.Scripts.GameLogic;

namespace App.ScriptsRefactoring.Game.GridAndCellsLogic
{
    public class CellResolver
    {
        public readonly Dictionary<string, Cell> NameToCell;

        public CellResolver(List<Cell> myCells)
        {
            NameToCell = new Dictionary<string, Cell>();

            foreach (var myCell in myCells)
            {
                NameToCell.Add(myCell.Name, myCell);
            }
        }
    }
}