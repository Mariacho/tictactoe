﻿using System;
using System.Collections;
using App.ScriptsRefactoring.Game.InputSystems;
using Photon.Pun;
using UnityEngine;

namespace App.ScriptsRefactoring.Game.GridAndCellsLogic
{
    public class PhotonTurnSender : MonoBehaviour
    {
        [SerializeField] private PhotonView _photonView;
        [SerializeField] private CellContainer _container;
        
        public PhotonInput Input;
        public CellResolver Resolver;

        private void Start()
        {
            StartCoroutine(CreateResolverAfterFrame());
        }

        IEnumerator CreateResolverAfterFrame()
        {
            yield return new WaitForEndOfFrame();
            
            Resolver = new CellResolver(_container.Cells);
        }

        public void Turn(string cellName)
        {
            Debug.Log($"Send cell name: {cellName}");
            _photonView.RPC("TryClickRemote", RpcTarget.All, cellName);
        }

        [PunRPC]
        public void TryClickRemote(string cellName)
        {
            var remoteCell = Resolver.NameToCell[cellName];
            Input.TryClickWithoutCondition(remoteCell);
        }
    }
}