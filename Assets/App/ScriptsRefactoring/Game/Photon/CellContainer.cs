﻿using System;
using System.Collections.Generic;
using App.Scripts.GameLogic;
using Photon.Pun;
using UnityEngine;

namespace App.ScriptsRefactoring.Game.GridAndCellsLogic
{
    [RequireComponent(typeof(PhotonView))]
    public class CellContainer : MonoBehaviour
    {
        [HideInInspector] public List<Cell> Cells = new List<Cell>();
        [SerializeField] private List<CellUnityContainer> _containers;
        
        private void Start()
        {
            foreach (var container in _containers)
            {
                Cells.Add(container.Cell);
            }
        }
        
        [ContextMenu("Find Containers")]
        private void FindContainers()
        {
            var tempConteiners = FindObjectsOfType<CellUnityContainer>();
            foreach (var tempConteiner in tempConteiners)
            {
                _containers.Add(tempConteiner);
            }
        }
    }
}