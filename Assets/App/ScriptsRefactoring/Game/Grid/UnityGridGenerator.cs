﻿using System;
using System.Collections.Generic;
using App.Scripts;
using App.Scripts.GameLogic;
using App.ScriptsRefactoring.Game.ScriptableObjects;
using Unity.Mathematics;
using UnityEngine;

namespace App.ScriptsRefactoring.Game
{
    public class UnityGridGenerator : MonoBehaviour
    {
        public float _offSetX, _offSetY;
        [SerializeField] private Vector2 _targetPosition;
        [SerializeField] private Transform _parantTransform;
        [SerializeField] private GridPreset _grid;
        [SerializeField] private GameObject _cell;

        [ContextMenu("Generate Grid")]
        private void GenerateGrid()
        {
            for (int i = 0; i < _grid.Rows; i++)
            {
                for (int j = 0; j < _grid.Lines; j++)
                {
                    var tempCell = Instantiate(_cell);
                    tempCell.transform.position = new Vector3(  _targetPosition.x + (_offSetX * i),
                                                                _targetPosition.y - (_offSetY * j),
                                                                0);
                    tempCell.transform.SetParent(_parantTransform);
                    tempCell.name = string.Format($"{i}-{j}");
                }
            }
        }
    }
}