﻿using System;
using System.Collections;
using App.Scripts.MessageSystem;
using UnityEngine;
using UnityEngine.UI;

namespace App.ScriptsRefactoring.Game
{
    public class ShowEndGame : MonoBehaviour
    {
        [SerializeField] private GameObject _windowEndGame;
        [SerializeField] private Text _resultGame;
        
        [Space(10)]
        [SerializeField] private UnityGameLoop _game;

        private void Start()
        {
            StartCoroutine(InitNextUpdate());
        }

        private void ShowResult(GameStateVariants resultStatus)
        {
            _windowEndGame.SetActive(true);
            _resultGame.text = resultStatus.ToString();
        }

        IEnumerator InitNextUpdate()
        {
            yield return new WaitForEndOfFrame();
            _game.GameLoop.GameState.ChangedState += ShowResult;
            MessagingSystem.Instance.QueueMessage(new EndGameMessage());
        }
    }
}