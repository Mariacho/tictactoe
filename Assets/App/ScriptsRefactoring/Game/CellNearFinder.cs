﻿using System;
using System.Collections.Generic;
using App.Scripts;
using App.Scripts.GameLogic;
using App.ScriptsRefactoring.Game;
using Unity.Mathematics;
using UnityEngine;

namespace App.ScriptsRefactoring.Game
{
    [RequireComponent(typeof(CellClickHandler))]
    public class CellNearFinder : MonoBehaviour
    {
        [SerializeField] private UnityGridGenerator _GridGenerator;
        private Dictionary<int, Vector2> _nearCoordinats;
        private Dictionary<int, int2> _nearIndexs;
        [SerializeField] private CellUnityContainer _cellContainer;
        private Cell _cell;

        private void Start()
        {
            FindNearCells();
        }

        private void FindNearCells()
        {
            Init();
            CalculateNear();
        }
        
        private void Init()
        {
            _cell = _cellContainer.Cell;
            
            var _offSetX = _GridGenerator._offSetX;
            var _offSetY = _GridGenerator._offSetY;
            
            _nearCoordinats = new Dictionary<int, Vector2>
            {
                {0, new Vector2(-_offSetX, _offSetY)},
                {1, new Vector2(-_offSetX, 0)},
                {2, new Vector2(-_offSetX, -_offSetY)},
                {3, new Vector2(0, _offSetY)},
                {4, new Vector2(0, 0)},
                {5, new Vector2(0, -_offSetY)},
                {6, new Vector2(_offSetX, _offSetY)},
                {7, new Vector2(_offSetX, 0)},
                {8, new Vector2(_offSetX, -_offSetY)}
            };
            
            _nearIndexs = new Dictionary<int, int2>
            {
                {0, new int2(0, 0)},
                {1, new int2(0, 1)},
                {2, new int2(0, 2)},
                {3, new int2(1, 0)},
                {4, new int2(1, 1)},
                {5, new int2(1, 2)},
                {6, new int2(2, 0)},
                {7, new int2(2, 1)},
                {8, new int2(2, 2)}
            };
        }

        private void CalculateNear()
        {
            Vector2 myPosition = transform.position;
            
            for (int i = 0; i < 9; i++)
            {
                var neighbour = TryRaycast(_nearCoordinats[i] + myPosition, myPosition);
                if (neighbour != null)
                    _cell.NearCells[_nearIndexs[i].x, _nearIndexs[i].y] = neighbour;
                else
                    _cell.NearCells[_nearIndexs[i].x, _nearIndexs[i].y] = new Cell(Cell.CellState.locked, new NoneVizualize(), "unnamed");
            }
        }

        private Cell TryRaycast(Vector2 targetPos, Vector2 myPosition)
        {
            RaycastHit2D hit = Physics2D.Raycast(targetPos, myPosition, 0.01f);
            if (hit.transform != null)
            {
                var result = hit.transform.gameObject.GetComponent<CellUnityContainer>();
                return result.Cell != null ? result.Cell : null;
            }
            return null;
        }
        
    }
}