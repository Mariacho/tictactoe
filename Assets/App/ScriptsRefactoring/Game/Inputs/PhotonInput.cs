﻿using App.Photon;
using App.Scripts.GameLogic;
using App.ScriptsRefactoring.Game.GridAndCellsLogic;
using Photon.Pun;
using UnityEngine;

namespace App.ScriptsRefactoring.Game.InputSystems
{
    public class PhotonInput : BaseInput
    {
        public PhotonTurnSender PhotonTurn;
        
        public PhotonInput()
        {
            if (PhotonNetwork.IsMasterClient)
                IsYourTurn = true;
            else
                IsYourTurn = false;
        }

        public override void TryClickWithCondition(Cell cell)
        {
            base.TryClickWithCondition(cell);
            
            if (IsYourTurn)
            {
                PhotonTurn.Turn(cell.Name);
                IsYourTurn = false;
            }
        }

        public override void TryClickWithoutCondition(Cell cell)
        {
            base.TryClickWithoutCondition(cell);
            IsYourTurn = true;
        }
    }
}