﻿using App.Scripts.GameLogic;

namespace App.ScriptsRefactoring.Game.InputSystems
{
    public class SoloInput : BaseInput
    {
        public SoloInput(bool isYourTurn)
        {
            IsYourTurn = isYourTurn;
        }

        public override void TryClickWithCondition(Cell cell)
        {
            TryClickWithoutCondition(cell);
        }
    }
}