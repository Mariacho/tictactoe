﻿using App.Scripts.GameLogic;
using Photon.Pun;

namespace App.ScriptsRefactoring.Game.InputSystems
{
    public abstract class BaseInput
    {
        public delegate void ClickCell(Cell cell);
        public event ClickCell TryMakeTurn;
        public bool IsYourTurn;

        public virtual void TryClickWithCondition(Cell cell)
        {
            if (IsYourTurn) 
                TryMakeTurn(cell);
        }

        public virtual void TryClickWithoutCondition(Cell cell)
        {
            TryMakeTurn(cell);
        }
    }
}