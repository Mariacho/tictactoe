﻿using App.ScriptsRefactoring.Game.GridAndCellsLogic;
using App.ScriptsRefactoring.Game.InputSystems;
using UnityEngine;

namespace App.ScriptsRefactoring.Game
{
    public class UnityGameLoop : MonoBehaviour
    {
        [SerializeField] private InputSelector _inputSelector;
        public IConditionOfTheFirst _conditionOfTheFirst;
        private BaseInput _currentInput;

        //public AudioCollection AudioCollection;
        public GameLoop GameLoop { get; private set; }

        private void Start()
        {
            _currentInput = _inputSelector.CurrentInput;
            if (_conditionOfTheFirst != null)
                GameLoop = new GameLoop(_currentInput, _conditionOfTheFirst.GetFirst());
            else
                GameLoop = new GameLoop(_currentInput, true);
        }
    }
}