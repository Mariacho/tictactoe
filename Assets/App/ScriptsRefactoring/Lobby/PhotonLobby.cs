﻿using System.Collections;
using App.Scripts.MessageSystem;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine;

namespace App.Photon
{
    public class PhotonLobby : MonoBehaviourPunCallbacks
    {
        public static bool isConnected;

        [SerializeField] private float _waitingTime = 0;
        
        public void Awake()
        {
            MessagingSystem.Instance.QueueMessage(new ConnectInformationMessage("Photon starting.."));
            
            PhotonNetwork.AutomaticallySyncScene = false;
            StartCoroutine(TryConnect());
        }

        IEnumerator TryConnect()
        {
            yield return new WaitForSeconds(0.2f);
            ConnectPhotonServer();
        }

        IEnumerator TryCreateOrJoin()
        {
            Debug.Log("<color=Yellow>MultiplayerRework</color>: IE TryCreateOrJoin");
            yield return new WaitUntil(() => isConnected);
 
            JoinRandomRoom();
        }
        
        public override void OnConnectedToMaster()
        {
            MessagingSystem.Instance.QueueMessage(new ConnectInformationMessage("Connected Photon server!"));
            isConnected = true;
            JoinRandomRoom();
        }
        
        public override void OnJoinRandomFailed(short returnCode, string message)
        {
            string roomName = "Room " + Random.Range(0, 10000);
            RoomOptions options = new RoomOptions { MaxPlayers = 2 };
            PhotonNetwork.CreateRoom(roomName, options, null);
        }
        
        public override void OnJoinedRoom()
        {
            MessagingSystem.Instance.QueueMessage(new ConnectInformationMessage("Finding opponent.."));
            StartCoroutine(TimeTick());
        }

        public override void OnPlayerEnteredRoom(Player newPlayer)
        {
            MessagingSystem.Instance.QueueMessage(new ConnectInformationMessage("Opponent found!"));
        }

        public override void OnPlayerLeftRoom(Player otherPlayer)
        {
            Debug.Log("<color=Yellow>MultiplayerRework</color>: OnPlayerLeftRoom");
        }
        
        public void LeaveLobby()
        {
            if (PhotonNetwork.InLobby)
            {
                PhotonNetwork.LeaveLobby();
            }
        }

        public void LeaveRoom()
        {
            if (PhotonNetwork.InRoom)
            {
                PhotonNetwork.LeaveRoom();
            }
            else
            {
                PhotonNetwork.Disconnect();
            }
        }
        
        public override void OnLeftRoom()
        {
            PhotonNetwork.Disconnect();
        }
        
        public override void OnDisconnected(DisconnectCause cause)
        {
            isConnected = false;
            MessagingSystem.Instance.QueueMessage(new LoadSceneMessage("Menu", false, false));
        }
        
        public void JoinRandomRoom()
        {
            MessagingSystem.Instance.QueueMessage(new ConnectInformationMessage("Finding room.."));
            PhotonNetwork.JoinRandomRoom();
        }

        public void ConnectPhotonServer()
        {
            MessagingSystem.Instance.QueueMessage(new ConnectInformationMessage("Try connect Photon server.."));
            string playerName = "Player" + Random.Range(0, 10000).ToString();
            PhotonNetwork.LocalPlayer.NickName = playerName;
            PhotonNetwork.ConnectUsingSettings();
        }

        public void JoinLobby()
        {
            if (!PhotonNetwork.InLobby)
            {
                PhotonNetwork.JoinLobby();
            }
        }

        public void StartGameWithPlayer()
        {
            PhotonNetwork.CurrentRoom.IsOpen = false;
            PhotonNetwork.CurrentRoom.IsVisible = false;
            
            MessagingSystem.Instance.QueueMessage(new ConnectInformationMessage("Opponent found! Load level.."));
            MessagingSystem.Instance.QueueMessage(new LoadSceneMessage("Game", false, false));
        }

        public void StartGameWithAi()
        {
            PhotonNetwork.CurrentRoom.IsOpen = false;
            PhotonNetwork.CurrentRoom.IsVisible = false;

            //Load Scene AI
        }

        IEnumerator TimeTick()
        {
            float time = _waitingTime;
            while (time >= 0)
            {
                yield return new WaitForSeconds(1f);
                time -= 1f;

                if (CheckLobby())
                {
                    StartGameWithPlayer();
                    yield break;
                }
            }
            
            //TODO: Добавить переход на сцену с AI
            LeaveRoom();
            //StartGameWithAi();
        }

        private bool CheckLobby()
        {
            return PhotonNetwork.CurrentRoom.PlayerCount == 2;
        }
    }
}