﻿using App.Scripts.MessageSystem;
using UnityEngine;
using UnityEngine.UI;

namespace App.Scripts.UI
{
    public class ConnectionInformer : MonoBehaviour
    {
        private Text _text;

        private void Awake()
        {
            var findUILink = FindObjectsOfType<FindUILinks>();
            
            if (findUILink.Length == 1)
                _text = findUILink[0].ConnectionInfo;
            else
                Debug.LogError("Error! Check GameUILinks on scene!");
        }

        void Start()
        {
            MessagingSystem.Instance.AttachListener(typeof(ConnectInformationMessage), this.HandleConnectInformationMessage);
        }
        
        private void OnDestroy()
        {
            MessagingSystem.Instance.DetachListener(typeof(ConnectInformationMessage), this.HandleConnectInformationMessage);
        }

        bool HandleConnectInformationMessage(BaseMessage msg)
        {
            var castMsg = (ConnectInformationMessage) msg;
            _text.text = castMsg.Message;
            return true;
        }


        

    }
}