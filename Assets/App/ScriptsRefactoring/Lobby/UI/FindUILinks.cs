﻿using UnityEngine;
using UnityEngine.UI;

namespace App.Scripts.UI
{
    public class FindUILinks : MonoBehaviour
    {
        [Header("FindingScreen")]
        public Button Back;
        public Text ConnectionInfo;
    }
}