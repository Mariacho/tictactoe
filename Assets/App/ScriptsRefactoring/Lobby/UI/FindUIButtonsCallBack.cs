﻿using App.Photon;
using App.Scripts.MessageSystem;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine;

namespace App.Scripts.UI
{
    public class FindUIButtonsCallBack : MonoBehaviour
    {
        [SerializeField] private PhotonLobby _photonLobby;
        [SerializeField] private FindUILinks _findUILinks;
        
        private void Awake()
        {
            _findUILinks.Back.onClick.AddListener(() => ClickBack());
        }
        
        private void ClickBack()
        {
            _photonLobby.LeaveRoom();
        }
        
    }
}